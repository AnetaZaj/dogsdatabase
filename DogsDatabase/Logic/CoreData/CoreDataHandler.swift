//
//  CoreDataHandler.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 03.11.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHandler: NSObject {
    
    private let fetchRequest: NSFetchRequest<Dog> = Dog.fetchRequest()
    
    private let sortDescriptor: NSSortDescriptor = {
        let sort = NSSortDescriptor(key: #keyPath(Dog.name), ascending: true)
        return sort
    }()
    
    //MARK: - Context
    
    private func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    private func saveContext() -> Bool {
        let context = getContext()
        do {
            try context.save()
            return true
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }
    
    //MARK: - Fetch
    
    func fetchData(withFetchRequest currentFetchRequest: NSFetchRequest<Dog>? = nil) -> [Dog]? {
        
        let currentFetchRequest = currentFetchRequest ?? self.fetchRequest
        
        currentFetchRequest.sortDescriptors = [self.sortDescriptor]
        
        let context = getContext()
        var dogs:[Dog]? = nil
        do {
            dogs = try context.fetch(currentFetchRequest)
            return dogs
        } catch {
            print("Error during fetching data!")
        }
        return nil
    }
    
    //MARK: - Save
    
    func saveDog(dogData: DogData) -> Bool {
        
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Dog", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(dogData.id, forKey: "id")
        manageObject.setValue(dogData.breed, forKey: "breed")
        manageObject.setValue(dogData.name, forKey: "name")
        manageObject.setValue(dogData.photoType?.rawValue, forKey: "photoType")
        manageObject.setValue(dogData.dogDescription, forKey: "longDescription")
        
        return saveContext()
    }
    
    func saveDogs(dogsData: [DogData]) -> Bool {
        var areDogsSaveCorrectly = true
        for dogData in dogsData {
            if !saveDog(dogData: dogData) {
                areDogsSaveCorrectly = false
            }
        }
        return areDogsSaveCorrectly
    }
    
    //MARK: - Delete
    
    func deleteDog(withId dogId: Int){
        let fetchRequest: NSFetchRequest<Dog> = Dog.fetchRequest()
        fetchRequest.predicate = NSPredicate.init(format: "id==\(dogId)")
        
        let context = getContext()
        if let result = fetchData(withFetchRequest: fetchRequest) {
            for object in result {
                context.delete(object)
            }
        }
        saveContext()
    }
    
    private func deleteAllCoreDataRecords() {
        let context = getContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dog")
        let deleteAllRequest = NSBatchDeleteRequest(fetchRequest: request)
        do {
            try context.execute(deleteAllRequest)
        }
        catch {
            print(error)
        }
        saveContext()
    }
    
    //MARK: - Update
    
    func updateDatabase(withDogsData dogsData: [DogData]) {
        
        deleteAllCoreDataRecords()
        saveDogs(dogsData: dogsData)
        
    }
}
