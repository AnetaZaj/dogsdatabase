//
//  DogData.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 29.10.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit

class DogData {
    var id: Int!
    var breed: String!
    var name: String!
    var photoType: DogPhotoTypeEnum?
    var dogDescription: String?
    
    lazy var dogImage: UIImage = {
        if self.photoType == nil {
            return UIImage(named: "no_photo")!
        } else {
            return UIImage(named: self.photoType!.rawValue)!
        }
    }()
    
    init(withDictionary dictionary: [String: Any]?) {
        guard let dictionary = dictionary else { return }
        guard let id = dictionary["ID"] as? Int else { return }
        guard let breed = dictionary["breed"] as? String else { return }
        guard let name = dictionary["name"] as? String else { return }
        
        let dogDescription = dictionary["description"] as? String
        if let photoTypeString = dictionary["photoType"] as? String {
            self.photoType = DogPhotoTypeEnum.init(rawValue: photoTypeString)
        } else {
            self.photoType = nil
        }
        
        self.id = id
        self.breed = breed
        self.name = name
        self.dogDescription = dogDescription
    }
    
    init(withCoreDataRecord dog: Dog?) {
        guard let dog = dog else { return }
        guard let breed = dog.breed else { return}
        guard let name = dog.name else { return }
        if let photoTypeString = dog.photoType {
            self.photoType = DogPhotoTypeEnum.init(rawValue: photoTypeString)
        } else {
            self.photoType = nil
        }
        
        self.id = Int(dog.id)
        self.name = name
        self.breed = breed
        self.dogDescription = dog.longDescription
    }
}

enum DogPhotoTypeEnum: String {
    case clean
    case cute
    case chaste
}
