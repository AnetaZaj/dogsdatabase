//
//  RoundedImageView.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 22.10.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedAndBorderedImageView: UIImageView {

    @IBInspectable
    public var cornerRadius: CGFloat = 5 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var borderColor: UIColor = .black {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
