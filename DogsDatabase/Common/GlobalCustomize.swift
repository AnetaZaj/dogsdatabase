//
//  NavigationControllerApperance.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 04.11.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit

class GlobalCustomize: NSObject {

    func setNavigationBarApperanceProperties() {
        
        let navigationBarApperance = UINavigationBar.appearance()
        navigationBarApperance.tintColor = UIColor.white
        navigationBarApperance.barTintColor = UIColor(red: 63/255, green: 109/255, blue: 168/255, alpha: 0.5)
        navigationBarApperance.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }
}
