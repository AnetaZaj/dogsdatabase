//
//  DogDetailsViewController.swift
//  DogsDatabase
//
//  Created by Aneta on 04.11.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit

class DogDetailsViewController: UIViewController {
    
    @IBOutlet weak var dogImageView: RoundedAndBorderedImageView!
    @IBOutlet weak var dogNameLabel: UILabel!
    @IBOutlet weak var dogBreedLabel: UILabel!
    @IBOutlet weak var dogDescriptionLabel: UILabel!
    
    var dogData: DogData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupControlls()
        self.title = dogData?.name
    }
    
    private func setupControlls() {
        if let data = dogData {
            dogNameLabel.text = data.name
            dogBreedLabel.text = data.breed
            dogDescriptionLabel.text = data.dogDescription
            dogImageView.image = data.dogImage
        }
    }
}
