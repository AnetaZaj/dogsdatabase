//
//  DogsListTableViewController.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 22.10.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit

class DogsListTableViewController: UITableViewController {
    
    private let model = DogsListViewModel()
    private var currentlySelectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Dog's database"
        addDownloadButtonToNavigationBar()
        
        self.model.loadDogsData()
        self.tableView.reloadData()
    }
    
    //MARK: - Bar button item
    
    private func addDownloadButtonToNavigationBar() {
        
        let downloadImage = UIImage(named: "download")
        let barButtonItem = UIBarButtonItem(image: downloadImage, style: .plain, target: self, action: #selector(redownloadDataFromDatabase))
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    //MARK: action
    
    @objc func redownloadDataFromDatabase() {
        model.redownloadDogsData()
        tableView.reloadData()
    }
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dogMainInfoCell", for: indexPath) as! DogMainInfoTableViewCell
        
        guard let currentDogData = model.getCurrentDogData(forIndexPath: indexPath) else { return UITableViewCell() }
        cell.nameLabel.text = currentDogData.name
        cell.breedLabel.text = currentDogData.breed
        cell.cellImageView.image = currentDogData.dogImage
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        model.dogChoosed(withIndexPath: indexPath)
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            deleteRowFromTableView(withIndexPath: indexPath)
        }
    }
    
    private func deleteRowFromTableView(withIndexPath indexPath: IndexPath) {
        model.removeRecordFromDatabase(withIndexPath: indexPath)
        
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }
    
    //MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! DogDetailsViewController
        destinationVC.dogData = model.currentlyChoosedDogData
    }
}
