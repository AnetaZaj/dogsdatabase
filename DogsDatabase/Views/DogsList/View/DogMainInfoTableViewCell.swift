//
//  DogMainInfoTableViewCell.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 22.10.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit

class DogMainInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellImageView: RoundedAndBorderedImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var breedLabel: UILabel!
    
}
