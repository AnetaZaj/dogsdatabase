//
//  DogsListViewModel.swift
//  RecruitmentTaskTSH
//
//  Created by Aneta on 22.10.2017.
//  Copyright © 2017 Aneta Zając. All rights reserved.
//

import UIKit
import CoreData

class DogsListViewModel {
    
    private var dogsData: [DogData]?
    private let coreDataHandler = CoreDataHandler()
    var currentlyChoosedDogData: DogData?
    
    func dogChoosed(withIndexPath indexPath: IndexPath){
        if let currentlyChoosedDogData = getCurrentDogData(forIndexPath: indexPath) {
            self.currentlyChoosedDogData = currentlyChoosedDogData
        }
    }
    
    //MARK: - Table view getters
    
    func getCurrentDogData(forIndexPath indexPath: IndexPath) -> DogData? {
        guard let dogsData = self.dogsData else { return nil }
        
        let row = indexPath.row
        if dogsData.count > row {
            return dogsData[row]
        }
        return nil
    }
    
    func numberOfRows() -> Int {
        if dogsData != nil {
            return dogsData!.count
        }
        return 0
    }
    
    // MARK: Database handle
    
    private func getDataFromJsonFile() -> [DogData]? {
        do {
            guard let file = Bundle.main.url(forResource: "DogsInitialDatabase", withExtension: "json") else { return nil }
            let data = try Data(contentsOf: file)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            guard let objectsArray = json as? [Any] else { return nil }
            
            var dogsDataFromJson = [DogData]()
            for anyObject in objectsArray {
                if let object = anyObject as? [String: Any] {
                    dogsDataFromJson.append(DogData(withDictionary: object))
                }
            }
            return dogsDataFromJson.sorted(by: {$0.name < $1.name})
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    private func getDataFromCoreData() -> [DogData]? {
        guard let dogs = coreDataHandler.fetchData() else { return nil}
        var dogsDataFromCD = [DogData]()
        for dog in dogs {
            dogsDataFromCD.append(DogData(withCoreDataRecord: dog))
        }
        return dogsDataFromCD
    }
    
    func loadDogsData(){
        if areDataDownloadedBefore() {
            self.dogsData = getDataFromCoreData()
        } else {
            if let dogsDataFromJson = getDataFromJsonFile() {
                self.dogsData = dogsDataFromJson
                if coreDataHandler.saveDogs(dogsData: dogsDataFromJson) {
                    saveInUserDefaultsThatFirstDownloadCompleted()
                }
            }
        }
    }
    
    func redownloadDogsData() {
        if let dogsDataFromJson = getDataFromJsonFile() {
            self.dogsData?.removeAll()
            coreDataHandler.updateDatabase(withDogsData: dogsDataFromJson)
            self.dogsData = dogsDataFromJson
        }
    }
    
    func removeRecordFromDatabase(withIndexPath indexPath: IndexPath) {
        guard let currentDogData = self.getCurrentDogData(forIndexPath: indexPath) else { return }
        if let dogIdToDelete = currentDogData.id {
            coreDataHandler.deleteDog(withId: dogIdToDelete)
        }
        self.dogsData = getDataFromCoreData()
    }
    
    //MARK: - User Defaults
    
    private func areDataDownloadedBefore() -> Bool {
        let dogsSaveInCoreData = UserDefaults.standard.bool(forKey: "savedDataInCoreDataBefore")
        return dogsSaveInCoreData
    }
    
    private func saveInUserDefaultsThatFirstDownloadCompleted() {
        UserDefaults.standard.set(true, forKey: "savedDataInCoreDataBefore")
    }
}
